<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Test - KDigital</title>
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	</head>
	<header class="pt-3 mb-5">
		<div class="container">
			<div class="row">
				<div class="col-md-12 d-flex flex-column align-items-center">
					<img src="assets/image/logo/liva.png" alt="">
					<h5>Brillian AV - Frontend Developer Technical Test</h5>
				</div>
			</div>
		</div>
		<div class="menu-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<nav class="navbar navbar-expand-lg">
							<div class="container-fluid">
								<a class="navbar-brand d-block d-md-none" href="#">Navbar</a>
								<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
									<span class="navbar-toggler-icon"></span>
								</button>
								<div class="collapse navbar-collapse" id="navbarSupportedContent">
									<ul class="navbar-nav mx-auto mb-2 mb-lg-0">
										<li class="nav-item">
											<a class="nav-link active" aria-current="page" href="#">Home</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="#">Link</a>
										</li>
                              <li class="nav-item">
											<a class="nav-link" href="#">Link</a>
										</li>
                              <li class="nav-item">
											<a class="nav-link" href="#">Link</a>
										</li>
									</ul>
								</div>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>

	<body>
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h2>Top Stories</h2>
            </div>
         </div>

         <form name="get_id" action="detail.php" method="POST">
         <input type="hidden" name="news_input" id="news_id">
         </form>

         <div class="row" id="news_list"></div>
      </div>

	  <!-- loading -->
	  	<div class="modal fade" id="loading-modal" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Loading . . .</h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
				</div>
			</div>
		</div>
   </body>

</html>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/main.js"></script> 