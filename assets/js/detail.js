$(document).ready(function () {
  var id = $("#news_id").html();

  var urlList = "https://hacker-news.firebaseio.com/v0/item/" + id + ".json";

  $.getJSON(urlList, function (data) {
    var unixTimeStamp = data.time;
    var timestampInMilliSeconds = unixTimeStamp * 1000;
    var date = new Date(timestampInMilliSeconds);

    var day = (date.getDate() < 10 ? "0" : "") + date.getDate();
    var month = (date.getMonth() < 9 ? "0" : "") + (date.getMonth() + 1);
    var year = date.getFullYear();

    var hours =
      ((date.getHours() % 12 || 12) < 10 ? "0" : "") +
      (date.getHours() % 12 || 12);
    var minutes = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
    var meridiem = date.getHours() >= 12 ? "pm" : "am";

    var formattedDate =
      day +
      "-" +
      month +
      "-" +
      year +
      " / " +
      hours +
      ":" +
      minutes +
      " " +
      meridiem;
    var newsDetail =
      "<h6>" +
      formattedDate +
      "</h6><h4>" +
      data.title +
      '</h4><ul class="list-inline">' +
      '<li class="list-inline-item">' +
      '<i class="fa fa-user" aria-hidden="true"></i> ' +
      data.by +
      "</li>" +
      '<li class="list-inline-item">' +
      '<i class="fa fa-commenting" aria-hidden="true"></i> ' +
      data.descendants +
      " Comments</li>" +
      '<li class="list-inline-item">' +
      '<i class="fa fa-star" aria-hidden="true"></i> ' +
      data.score +
      "</li> " +
      "</ul>" +
      '<div class="detail mt-5">' +
      data.text +
      "</div>";

    $("#news_detail").append(newsDetail);

    $.each(data.kids, function (i, l) {
      $.getJSON(
        "https://hacker-news.firebaseio.com/v0/item/" + l + ".json",
        function (comment) {
          var unixTimeStamp = comment.time;
          var timestampInMilliSeconds = unixTimeStamp * 1000;
          var date = new Date(timestampInMilliSeconds);

          var day = (date.getDate() < 10 ? "0" : "") + date.getDate();
          var month = (date.getMonth() < 9 ? "0" : "") + (date.getMonth() + 1);
          var year = date.getFullYear();

          var hours =
            ((date.getHours() % 12 || 12) < 10 ? "0" : "") +
            (date.getHours() % 12 || 12);
          var minutes = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
          var meridiem = date.getHours() >= 12 ? "pm" : "am";

          var formattedDate =
            day +
            "-" +
            month +
            "-" +
            year +
            " / " +
            hours +
            ":" +
            minutes +
            " " +
            meridiem;
          var comments =
            '<div class="comments-area p-3">' +
            '<div class="top-area d-flex align-items-center">' +
            '<img src="assets/image/user.png" alt="">' +
            '<div class="name-comments ml-3"><h4>' +
            comment.by +
            "</h4><h6>" +
            formattedDate +
            "</h6></div>" +
            "</div>" +
            '<div class="bottom-area">' +
            comment.text +
            "</div>" +
            "</div>";
            $("#comments").append(comments);
        }
      );
    });
  });
});
