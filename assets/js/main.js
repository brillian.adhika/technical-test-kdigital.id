$(document).ready(function () {
  var items = [];
  $.getJSON(
    "https://hacker-news.firebaseio.com/v0/topstories.json",
    function (data) {
      $.each(data, function (key, val) {
        getList(val);
      });
    }
  );

  var urlList = "https://hacker-news.firebaseio.com/v0/item/";

  function getList(params) {
    $.getJSON(urlList + params + ".json", function (data) {
      var unixTimeStamp = data.time;
      var timestampInMilliSeconds = unixTimeStamp * 1000;
      var date = new Date(timestampInMilliSeconds);

      var day = (date.getDate() < 10 ? "0" : "") + date.getDate();
      var month = (date.getMonth() < 9 ? "0" : "") + (date.getMonth() + 1);
      var year = date.getFullYear();

      var hours =
        ((date.getHours() % 12 || 12) < 10 ? "0" : "") +
        (date.getHours() % 12 || 12);
      var minutes = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
      var meridiem = date.getHours() >= 12 ? "pm" : "am";

      var formattedDate =
        day +
        "-" +
        month +
        "-" +
        year +
        " / " +
        hours +
        ":" +
        minutes +
        " " +
        meridiem;
      var newsList =
        '<div class="col-md-6 mb-3">' +
        '<div class="news-list p-3">' +
        "<h6>" +
        formattedDate +
        "</h6>" +
        '<a href="#" class="detaillist" id="' +
        params +
        '"><h4>' +
        data.title +
        "</h4></a>" +
        '<ul class="list-inline">' +
        '<li class="list-inline-item">' +
        '<i class="fa fa-commenting" aria-hidden="true"></i> ' +
        data.descendants +
        "</li>" +
        '<li class="list-inline-item">' +
        '<i class="fa fa-star" aria-hidden="true"></i> ' +
        data.score +
        "</li> " +
        "</ul>" +
        "</div>" +
        "</div>";

      $("#news_list").append(newsList);

      $("#" + params).click(function () {
        getDetail(params);
      });
    });
  }

  // var element = $('#news_list').find('.detaillist');

  var loadingModal = new bootstrap.Modal(
    document.getElementById("loading-modal"),
    {
      keyboard: false,
    }
  );

  function getDetail(params) {
    // loadingModal.show()

    // var bodyResponse;
    // $.getJSON(urlList + params + ".json", function(response) {
    //     loadingModal.hide()
    //     bodyResponse = response;
    //     console.log(bodyResponse);
    // })

    $("#news_id").attr("value", params);
    $("form[name='get_id']").submit();

    $.ajax({
      type: "POST",
      url: "detail.php",
      data: { taskid: $("input[name='news_id']").val() },
    });
  }
});
